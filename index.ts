import express, {
  Application, Request, Response,
} from "express";
import dotenv from "dotenv";
import cors from 'cors';
import { BaseRouter } from "./router/base-routes";
import { PlaylistRouter } from "./controller/playlist";
export const app: Application = express();
const port = process.env.PORT || 8000;
dotenv.config();

app.use(express.json()).use(cors());
app.use(BaseRouter).use(PlaylistRouter)
app.listen(port, () => {
  console.log(`Server running in http://localhost:${port}`);
});
