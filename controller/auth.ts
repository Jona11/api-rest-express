import { Request, Response, query } from "express";
import { pool } from "../utils/connection-service";
const { map } = require("async");
import dayjs from "dayjs";
import { executeQuery, isModelValid } from "../utils/base-service";

// script generate token
const token = (): string => require("crypto").randomBytes(64).toString("hex");

const login = async (email: string, password: string) => {
  // execute query if user is valid
  try {
    const data = await pool.query(
      "select * from accounts ac where ac.email = $1 and ac.password = $2;",
      [email, password]
    );

    // return jwt
    const jwt = data.rowCount > 0 ? `${token()}` : null;

    const query = `update accounts ac set token = '${jwt}' where ac.email = '${email}' and ac.password = '${password}' RETURNING *;`;
    try {
      const { data: newData } = await executeQuery(query);
      return { jwt: jwt, user: newData[0] };
    } catch {
      return { jwt: jwt, user: {} };
    }
  } catch (error) {
    return null;
  }
};

export const getMeService = async (request: Request, response: Response) => {
  const header = request.get('Authorization')
  const token = header?.slice(7)
  const query = `select * from accounts ac where ac.token = '${token}'`
  const { data }= await executeQuery(query)
  return response.send({ data: data[0] })
}

export const authService = async (request: Request, response: Response) => {
  const data = request.body.data;
  const email = data ? data.email : request.body.identifier
  const password = data ? data.password : request.body.password

  try {
    // get login service
    return response.send(await login(email, password));
  } catch (error) {
    return response.send({ message: "Error" });
  }
};

const storeModel = async (modelTable: string, model: Object) => {
  const keys = Object.keys(model);
  const data = Object.values(model);

  if (await isModelValid(modelTable)) {
    const querySave = `INSERT INTO ${modelTable} (${keys.join(
      ", "
    )}) VALUES(${data
      .map((_, index) => `$${index + 1}`)
      .join(", ")}) RETURNING *;`;

    try {
      const result = await pool.query(
        querySave,
        data /* , (results: any, error: any) => {
            console.log(results)
            console.log(error)
          } */
      );
      return { data: result.rows[0] };
    } catch (error) {
      return { data: null };
    }
  }
  return { data: null };
};

export const storeCustomService = async (
  request: Request,
  response: Response
) => {
  const account = request.body.data.account;
  const user_card = request.body.data.user_card;
  //const subscription = request.body.data.subscription;
  const pay = request.body.data.payment_type;

  const isPremium = !!pay

  delete account.id
  delete account.role
  try {
    // saved account
    account.type_account = isPremium ? 'premium': 'regular'
    const { data: userSaved } = await storeModel("accounts", account);
    //const userSaved = { id: 7, name: "JUan perez" };
    user_card.user_id = userSaved.id;

    //if exist pay saved details of pay
    if (!!isPremium) {
      // if is paypal
      const isPaypal = pay === "PAYPAL";
      // if is type payment paypal saved name user
      const subscription = {
        user_id: userSaved.id,
        user_name_paypal: isPaypal ? userSaved.name : "",
        type_payment: pay,
        date_update: dayjs(new Date()).format("YYYY-MM-DD"),
      };

      //saved subscription
      // saved payment of the user
      const subscriptionSaved = await storeModel("subscription", subscription);
      const payment = {
        date_payment: dayjs(new Date()).format("YYYY-MM-DD"),
        number_order: userSaved.id,
        total: +(100).toFixed(2),
        user_id: userSaved.id,
      };
      const paymentSaved = await storeModel("payment", payment);
      //const card =
      delete user_card.id
      pay === "CARD" ? await storeModel("user_card", user_card) : null;

      const loginReponse = await login(userSaved.email, userSaved.password)

      // return data with all details
      return response.send({
        user: userSaved,
        jwt: loginReponse?.jwt,
        message: "Subscription Success!",
        subscription: subscriptionSaved,
        payment: paymentSaved,
        //card,
      });
      //return response.send({ data: null, message: "Data Not Saved" });
    }
    return response.send({ data: userSaved });
  } catch {
    return response.send({ data: null, message: "Data Not Saved" });
  }
};

// function get all data of a user by id
export const getAllDataCustom = async (
  request: Request,
  response: Response
) => {
  const token = request.headers["jwt"];

  //query and set data in user model
  const query = `select * from accounts ac where ac.token = '${token}';`;

  try {
    const { data, size } = await executeQuery(query);
    const accountId = data[0].id;
    const querySubscription = `select * from subscription s where s.user_id = ${accountId};`;
    const queryPayment = `select * from payment s where s.user_id = ${accountId};`;
    const queryPlaylist = `select * from playlist s where s.user_id = ${accountId};`;

    //si exist user: result 1
    if (size > 0) {
      const subscriptions = await executeQuery(querySubscription);
      const payments = await executeQuery(queryPayment);
      const playlists = await executeQuery(queryPlaylist);

      //each songs
      await map(
        playlists.data,
        async (playlist: {
          total_songs: any;
          songsLikes: { data: any; size: any };
          songs: { data: any; size: any };
          id: any;
        }) => {
          const querySong = `select * from songs s where s.playlist_id =${playlist.id};`;
          const querySongLikes = `select * from songs s where s.like_active = true`;
          const songs = await executeQuery(querySong);
          
          playlist.songs = songs
          playlist.total_songs = songs.size 
          playlist.songsLikes = await executeQuery(querySongLikes);
          return playlist;
        }
      );

      return response.send({
        data: {
          user: {
            data: data[0],
          },
          payments,
          playlists,
          subscriptions,
        },
        message: "Success",
      });
    } else {
      return response.send({ data: {}, message: "User Invalid" });
    }
  } catch (error) {
    return response.send({ data: null, message: "Data Not Found" });
  }
};
