import express, { Request, Response } from "express";
import { Model } from "../utils/types";

import {
  executeQuery,
  findByIdService,
  getAllDataService,
  isModelValid,
  updateModelService,
} from "../utils/base-service";

export const PlaylistRouter = express.Router();
const { map } = require("async");

//method custom for not deleted playlist
const customDelete = async (req: Request, response: Response) => {
  const tableModel = "playlist";
  // valid model in database
  if (!(await isModelValid(tableModel))) {
    // return invalid model
    return response.status(500).send({ message: "Invalid Model", data: [] });
  }

  try {
    // find paylist
    const { data: model } = await findByIdService("playlist", +req.params.id);
    delete model.id;
    delete model.code_unique;
    model.is_deleted = true;
    model.deleted_at = new Date();
    //update play list, and set deleted true
    await updateModelService(tableModel, model, +req.params.id);
    // return with successfully
    return response.send({
      message: "Playlist Deleted",
      data: await getAllDataService(tableModel),
    });
  } catch {
    // return error
    return response.send({ message: "Delete Failed" });
  }
};

export const getAllDataByModel = async (req: Request, response: Response) => {
  // check if exist param
  const is_deleted = !!req.params.deleted;
  // by default return playlist active
  const queryPlaylist = `select * from playlist p where p.is_deleted = ${is_deleted};`;
  try {
    //execute query
    const { data: playlists } = await executeQuery(queryPlaylist);
    //each playlist and set songs
    const allPlaylist = await map(playlists, async (playlist: Model) => {
      const query = ` select * from songs where playlist_id = ${playlist.id}`;
      const songs = await executeQuery(query);
      playlist.songs = songs;
      playlist.total_songs = songs.size
      return playlist;
    });
    // return data with successfuly
    return response.send({ data: allPlaylist, size: allPlaylist.length });
  } catch (error) {
    // return data with error
    return response.send({ data: [], message: "Get All Data With Error" });
  }
};

// custom endpoint
PlaylistRouter.delete(
  "/api/public/playlist/deleted/:id",
  (req: Request, res: Response) => customDelete(req, res)
);
PlaylistRouter.get(
  "/api/public/playlist/all/:deleted?",
  (req: Request, res: Response) => getAllDataByModel(req, res)
);
