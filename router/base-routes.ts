import express, { Request, Response, Router } from "express";
import { deleteById, findById, getAllData, storeData } from "../utils/connection-service";
import { authService, getAllDataCustom, storeCustomService, getMeService } from "../controller/auth";

export const BaseRouter = express.Router();

const baseAPI = "/api/public/:model"
BaseRouter.post("/api/auth/local", (request: Request, response: Response) => authService(request, response))
BaseRouter.get("/api/users/me", (request: Request, response: Response) => getMeService(request, response))

BaseRouter.post("/api/public/custom", (req: Request, res: Response) => storeCustomService(req, res))
BaseRouter.post("/api/public/account/get/", (req: Request, res: Response) => getAllDataCustom(req, res))

// base api
BaseRouter.get(`${baseAPI}`, (req: Request, res: Response) => getAllData(req, res));
BaseRouter.post(`${baseAPI}/:id`, (req: Request, res: Response) => findById(req, res));
BaseRouter.put(`${baseAPI}`, (req: Request, res: Response) => storeData(req, res));
BaseRouter.delete(`${baseAPI}/:id`, (req: Request, res: Response) => deleteById(req, res));


