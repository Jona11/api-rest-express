import { Request, Response } from "express";
import {
  database,
  deleteByIdService,
  findByIdService,
  getAllDataService,
  isModelValid,
} from "./base-service";

const Pool = require("pg").Pool;

// params of connection db
export const pool = new Pool({
  host: "192.168.3.100",
  user: "postgres",
  database,
  password: "root",
  port: 5432,
  allowExitOnIdle: true,
  max: 1000000000000000,
});

export const getAllData = async (request: Request, response: Response) => {
  const modelTable = request.params.model;

  // check if exist model
  if (!(await isModelValid(modelTable))) {
    return response.status(500).send({ message: "Invalid Model", data: [] });
  }
  return response.send(await getAllDataService(modelTable));
};

export const findById = async (request: Request, response: Response) => {
  const modelTable = request.params.model;
  const modelTableId = request.params.id;
  if (!(await isModelValid(modelTable))) {
    return response.status(500).send({ message: "Invalid Model", data: [] });
  }
  return response.send(
    await findByIdService(modelTable, modelTableId as unknown as number)
  );
};

export const deleteById = async (request: Request, response: Response) => {
  const modelTable = request.params.model;
  const modelTableId = +request.params.id;
  if (!(await isModelValid(modelTable))) {
    return response.status(500).send({ message: "Invalid Model", data: [] });
  }
  return response.send(await deleteByIdService(modelTable, modelTableId));
};

export const storeData = async (request: Request, response: Response) => {
  const modelTable = request.params.model;
  if (!(await isModelValid(modelTable))) {
    return response.status(500).send({ message: "Invalid Model", data: [] });
  }
  const modelData = request.body.data;
  const modelId = modelData?.id;
  try {
    if (modelId >= 0) {
      delete modelData.id;
    }
    const keys = Object.keys(modelData);
    const data = Object.values(modelData);

    const querySave = `INSERT INTO ${modelTable} (${keys.join(
      ", "
    )}) VALUES(${data
      .map((_, index) => `$${index + 1}`)
      .join(", ")}) RETURNING *;`;

    const queryUpdate = `UPDATE ${modelTable} SET ${keys
      .map((key, i) => `${key}=$${i + 1}`)
      .join(", ")} WHERE id=${modelId} RETURNING *;`;
    const query = modelId > 0 ? queryUpdate : querySave;

    const result = await pool.query(
      query,
      data /* ,
      (error: unknown, results: unknown) => {
        console.log(error);
        console.log(results);
      } */
    );

    return response.send({
      message: modelId > 0 ? "Updated Successfull" : "Saved Successfull",
      data: result.rows[0],
    });
  } catch (error) {}
  return response.status(500).send({ message: "Saved Failed", data: [] });
};
