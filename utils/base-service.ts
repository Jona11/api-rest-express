import { pool } from "./connection-service";
export const database = "spotify_db";
const pgp = require("pg-promise")();

// Configuración de la conexión a PostgreSQL
const db = pgp({
  host: "192.168.3.100",
  user: "postgres",
  database: "spotify_db",
  password: "root",
  port: 5432,
  allowExitOnIdle: true,
  max: 20,
});

export const executeQuery = async (query: string) => {
  const result = await pool.query(
    query /* ,  (error: any, results: any) => {
    console.log(error)
    console.log(results)
  } */
  );
  return { data: result.rows, size: result.rowCount };
};

// script create db
export const connectDataBase = async () => {
  const query = `SELECT datname FROM pg_catalog.pg_database WHERE datname = '${database}';`;

  const { size } = await executeQuery(query);

  if (size === 0) {
    console.log(`${pool.database} database not found, creating it.`);
    await pool.query(`CREATE DATABASE "${database}";`);
    console.log(`created database ${database}`);
  } else {
    console.log(`${database} database already exists.`);
  }
};

// check if exist table
export const isModelValid = async (tableName: string) => {
  try {
    await connectDataBase();
  } catch (error) {
    return false;
  }

  const result = await db.oneOrNone(
    `
SELECT table_name
FROM information_schema.tables
WHERE table_schema = 'public' AND table_name = $1;
`,
    [tableName]
  );

  pool.connect();
  return !!result;
};

export const getAllDataService = async (tableNameModel: string) => {
  try {
    const query = `SELECT * FROM ${tableNameModel} ORDER BY id ASC`;
    const { data, size } = await executeQuery(query);
    return {
      message: "All Data",
      data,
      meta: {
        pagination: { total: size },
      },
    };
  } catch (error) {}
  // if exist error, show message error
  return { message: "Get data with error", data: [] };
};

export const updateModelService = async (
  tableModel: string,
  model: Object,
  modelId: number
) => {
  const allData = async () => await getAllDataService(tableModel);
  try {
    const keys = Object.keys(model);
    const data = Object.values(model);

    const queryUpdate = `UPDATE ${tableModel} SET ${keys
      .map((key, i) => `${key}=$${i + 1}`)
      .join(", ")} WHERE id=${modelId} RETURNING *`;

    const result = await pool.query(
      queryUpdate,
      data,
      (error: unknown, results: unknown) => {
        console.log(error);
        console.log(results);
      }
    );
    return { message: "Playlist Updated", data: allData() };
  } catch (error) {
    return { message: "Error in Updated List", data: allData() };
  }
};

export const findByIdService = async (
  tableNameModel: string,
  tableModelId: number
) => {
  const query = `SELECT * FROM ${tableNameModel} WHERE id = ${tableModelId};`;
  try {
    const { data } = await executeQuery(query);
    return { message: "Get Successfull", data: data[0] };
  } catch (error) {}
  return { message: "Get data with error", data: [] };
};

export const deleteByIdService = async (
  tableNameModel: string,
  tableModelId: number
) => {
  const query = `DELETE FROM ${tableNameModel} WHERE id = ${tableModelId};`;
  try {
    const a = await executeQuery(query);
    return {
      message: "Deleted Successfull",
      data: await getAllDataService(tableNameModel),
    };
  } catch (error) {}
  return { message: "Deleted Failed", data: [] };
};
