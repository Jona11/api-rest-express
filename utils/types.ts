export interface Model {
    id: number;
    [x: string]: unknown;
  }